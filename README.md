# Restic log exporter

This exporter parses restic's logs (either from a logfile or the systemd journal)
and exports metrics from the last backup run.

It is ased on [this blog article](https://blog.cubieserver.de/2021/restic-backups-with-systemd-and-prometheus-exporter/#reporting-failures)
and adapted to work with current restic versions (tested with 0.16.3).

## Usage

Example usage with systemd timer or cronjob (requires "sponge" binary from
i.e. the "moreutils" debian package):

    restic-log-exporter.sh | sponge /var/lib/prometheus/node-exporter/restic-backup.prom

Example output:

    restic_backup_failure{unit="restic-all.service",timestamp="1663227870"} 0
    restic_duration_seconds{unit="restic-all.service",action="total"} 1122
    restic_last_successful_backup{unit="restic-all.service"} 1663227870
    restic_repo_dirs{unit="restic-all.service",state="changed"} 1073
    restic_repo_dirs{unit="restic-all.service",state="new"} 2
    restic_repo_dirs{unit="restic-all.service",state="unmodified"} 104334
    restic_repo_files{unit="restic-all.service",state="changed"} 168
    restic_repo_files{unit="restic-all.service",state="new"} 48
    restic_repo_files{unit="restic-all.service",state="total"} 735333
    restic_repo_files{unit="restic-all.service",state="unmodified"} 735117
    restic_repo_size_bytes{unit="restic-all.service",state="added"} 1927366574
    restic_repo_size_bytes{unit="restic-all.service",state="stored"} 871002800
    restic_repo_size_bytes{unit="restic-all.service",state="total"} 266086108889


## Todo

* Better deal with failed backups
* Add snapshot id (snapshot 30b9d571 saved)
* Add restic backup duration
* Rewrite in Python :)
