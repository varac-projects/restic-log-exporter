#!/bin/bash
#
# Based on https://blog.cubieserver.de/2021/restic-backups-with-systemd-and-prometheus-exporter/#reporting-failures
# Adapter to restic 1.6.3
#
# Usage in i.e. systemd timer:
#
#  restic-log-exporter.sh | sponge /var/lib/prometheus/node-exporter/restic-backup.prom
#
# Todo:
#   * Add snapshot id (snapshot 30b9d571 saved)
#   * Add restic backup duration

set -eEuo pipefail

UNIT='restic-all.service' # needs to include '.service' !
# list of labels attached to all series, comma separated, without trailing comma
COMMON_LABELS="unit=\"${UNIT}\""
LOGS=

function error_finalizer() {
  echo "# Something went wrong, trap captured and finalizing with error_finalizer() !"
  echo "restic_backup_failure{${COMMON_LABELS},timestamp=\"$(date '+%s')\"} 1"
}

trap "error_finalizer" ERR

function convert_to_bytes() {
  local value=$1
  local unit=$2
  local factor

  case $unit in
    'KiB')
      factor=1024
      ;;
    'KB')
      factor=1000
      ;;
    'MiB')
      factor=1048576
      ;;
    'MB')
      factor=1000000
      ;;
    'GiB')
      factor=1073741824
      ;;
    'GB')
      factor=1000000000
      ;;
    'TiB')
      factor=1099511627776
      ;;
    'TB')
      factor=1000000000000
      ;;
    *)
      echo "Unsupported unit $unit"
      return 1
      ;;
  esac

  awk 'BEGIN {printf "%.0f", '"${value}*${factor}"'}'
}

function analyze_files_line() {
  # example line:
  # Files:          68 new,    38 changed, 109657 unmodified
  local files_line new_files changed_files unmodified_files
  files_line=$(echo "$LOGS" | grep 'Files:' | cut -d':' -f4-)
  new_files=$(echo "$files_line" | awk '{ print $5 }')
  changed_files=$(echo "$files_line" | awk '{ print $7 }')
  unmodified_files=$(echo "$files_line" | awk '{ print $9 }')
  if [ -z "$new_files" ] || [ -z "$changed_files" ] || [ -z "$unmodified_files" ]; then
    # this line should be present, fail if its not
    return 1
  fi
  echo "restic_repo_files{${COMMON_LABELS},state=\"new\"} $new_files"
  echo "restic_repo_files{${COMMON_LABELS},state=\"changed\"} $changed_files"
  echo "restic_repo_files{${COMMON_LABELS},state=\"unmodified\"} $unmodified_files"
}

function analyze_dirs_line() {
  # Dirs:            0 new,     1 changed,     1 unmodified
  local files_line new_dirs changed_dirs unmodified_dirs
  files_line=$(echo "$LOGS" | grep 'Dirs:' | cut -d':' -f4-)
  new_dirs=""$(echo "$files_line" | awk '{ print $5 }')
  changed_dirs=$(echo "$files_line" | awk '{ print $7 }')
  unmodified_dirs=$(echo "$files_line" | awk '{ print $9 }')
  if [ -z "$new_dirs" ] || [ -z "$changed_dirs" ] || [ -z "$unmodified_dirs" ]; then
    # this line should be present, fail if its not
    return 1
  fi
  echo "restic_repo_dirs{${COMMON_LABELS},state=\"new\"} $new_dirs"
  echo "restic_repo_dirs{${COMMON_LABELS},state=\"changed\"} $changed_dirs"
  echo "restic_repo_dirs{${COMMON_LABELS},state=\"unmodified\"} $unmodified_dirs"
}

function analyze_added_line() {
  # Added to the repository: 4.902 GiB (2.573 GiB stored)
  local added_line added_value added_unit added_bytes stored_value stored_unit stored_bytes
  added_line=$(echo "$LOGS" | grep 'Added to the repository:' | cut -d':' -f5- | tr -d '()')
  added_value=$(echo "$added_line" | awk '{ print $5 }')
  added_unit=$(echo "$added_line" | awk '{ print $6 }')
  added_bytes=$(convert_to_bytes "$added_value" "$added_unit")
  if [ -z "$added_bytes" ]; then
    return 1
  fi
  stored_value=$(echo "$added_line" | awk '{ print $7 }')
  stored_unit=$(echo "$added_line" | awk '{ print $8 }')
  stored_bytes=$(convert_to_bytes "$stored_value" "$stored_unit")
  if [ -z "$stored_bytes" ]; then
    return 1
  fi
  echo "restic_repo_size_bytes{${COMMON_LABELS},state=\"added\"} $added_bytes"
  echo "restic_repo_size_bytes{${COMMON_LABELS},state=\"stored\"} $stored_bytes"
}

function analyze_repository_line() {
  # repository contains 23329 packs (291507 blobs) with 109.102 GiB
  # Not shown anymore in restic 0.14.0
  # Note: the "|| true" parts prevent bash from exiting due to PIPEFAIL

  repo_line=$(echo "$LOGS" | (grep 'repository contains' || true) | (cut -d':' -f4- || true))
  # this line only exists when also a prune was run
  if [ -n "$repo_line" ]; then
    repo_value=$(echo "$repo_line" | awk '{print $8 }')
    repo_unit=$(echo "$repo_line" | awk '{print $9 }')
    echo "# repo_value repo_unit: $repo_value $repo_unit"
    repo_bytes=$(convert_to_bytes "$repo_value" "$repo_unit")
    if [ -n "$repo_bytes" ]; then
      echo "restic_repo_size_bytes{${COMMON_LABELS},state=\"total\"} $repo_bytes"
    fi
  fi
}

function analyze_summary() {
  # Example line: processed 735043 files, 247.850 GiB in 15:55
  # Note: the "|| true" parts prevent bash from exiting due to PIPEFAIL

  local files_value summary_line summary_value summary_unit summary_bytes
  summary_line=$(echo "$LOGS" | (grep 'processed .* files' || true) | (cut -d':' -f4- || true))
  if [ -n "$summary_line" ]; then
    files_value=$(echo "$summary_line" | awk '{print $5 }')
    summary_value=$(echo "$summary_line" | awk '{print $7 }')
    summary_unit=$(echo "$summary_line" | awk '{print $8 }')
    summary_bytes=$(convert_to_bytes "$summary_value" "$summary_unit")
    if [ -n "$summary_bytes" ]; then
      echo "restic_repo_size_bytes{${COMMON_LABELS},state=\"total\"} $summary_bytes"
    fi
    echo "restic_repo_files{${COMMON_LABELS},state=\"total\"} $files_value"
  fi
}

function get_script_seconds() {
  local script_name script_logs
  script_name="$1"
  script_logs=$(echo "$LOGS" | (grep -s -F "$script_name" || true))
  if [ -z "$script_logs" ]; then
    return
  fi

  # example time format: 2019-03-03T01:39:22+0100
  start_time_seconds=$(date '+%s' -d "$(echo "$script_logs" | head -1 | awk '{ print $1 }')")
  stop_time_seconds=$(date '+%s' -d "$(echo "$script_logs" | tail -1 | awk '{ print $1 }')")
  duration_seconds=$((stop_time_seconds - start_time_seconds))
  echo "$duration_seconds"
}

function main() {
  local log_file id
  log_file="${1:-}"
  if [ -n "${log_file}" ]; then
    # get logs from file (useful for debugging / testing)
    LOGS="$(cat "$log_file")"
  else
    # get last invocation id
    # from: https://unix.stackexchange.com/a/506887/214474
    id=$(systemctl show -p InvocationID --value "$UNIT")
    if [ -z "$id" ]; then
      echo "# Unable to fetch systemd service ID for unit $UNIT"
      return 1
    fi

    # get logs from last invocation
    LOGS="$(journalctl -o short-iso INVOCATION_ID="$id" + _SYSTEMD_INVOCATION_ID="$id")"
  fi

  # echo $LOGS

  # check if unit failed
  if echo "$LOGS" | grep -F "systemd[1]: ${UNIT}: Failed with result"; then
    # jumps to error_finalizer
    return 1
  fi

  analyze_files_line
  analyze_dirs_line
  analyze_added_line
  # analyze_repository_line
  analyze_summary

  local backup_duration_seconds cleanup_duration_seconds prune_duration_seconds
  # script durations:
  duration_seconds=$(get_script_seconds 'backup-all.sh')
  if [ -n "$duration_seconds" ]; then
    echo "restic_duration_seconds{${COMMON_LABELS},action=\"total\"} $duration_seconds"
  fi

  # # cleanup
  # cleanup_duration_seconds=$(get_script_seconds 'cleanup-backups.sh')
  # if [ -n "$cleanup_duration_seconds" ]; then
  #     echo "restic_backup_duration_seconds{${COMMON_LABELS},action=\"cleanup\"} $cleanup_duration_seconds"
  # fi
  #
  # # prune
  # prune_duration_seconds=$(get_script_seconds 'restic-prune.sh')
  # if [ -n "$prune_duration_seconds" ]; then
  #     echo "restic_backup_duration_seconds{${COMMON_LABELS},action=\"prune\"} $prune_duration_seconds"
  # fi

  # everything ok
  timestamp=$(date '+%s')
  echo "restic_backup_failure{${COMMON_LABELS},timestamp=\"$timestamp\"} 0"
  echo "restic_last_successful_backup{${COMMON_LABELS}} $timestamp"

  return 0
}

main "$@"
